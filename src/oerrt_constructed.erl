%%% @author  Matthew
%%% @copyright (C) 2017 Matthew Bond
%%% @doc
%%% Realtime OER encoding and decoding of constructed ASN.1 Types
%%% @end
%%%-------------------------------------------------------------------
-module(oerrt_constructed).

-include_lib("asn1/src/asn1_records.hrl").

-type component_list():: [#'ComponentType'{}].
-type components() :: component_list()                     |
                      {component_list(), component_list()} |
                      {component_list(), component_list(), component_list()}.

-export([enc_sequence/2,     dec_sequence/2,
         enc_sequence_of/2,  dec_sequence_of/2,
         enc_set/2,          dec_set/2,
         enc_set_of/2,       dec_set_of/2,
         enc_choice/2,       dec_choice/2,
         enc_embedded_pdv/1, dec_embedded_pdv/1]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 SEQUENCE and returns the binary.
%% NTCIP 1102 Section 2.3.8
%% @end
%%--------------------------------------------------------------------
-spec enc_sequence(Data :: list(), Components :: components()) -> iolist().
enc_sequence(Data, Components) ->
    collection_enc(fun oerrt:encode_disp/2, Data, Components).

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 SEQUENCE binary and returns the decoded value and
%% remaining binary.
%% NTCIP 1102 Section 2.3.8
%% @end
%%--------------------------------------------------------------------
-spec dec_sequence(Content :: binary(), Components ::components()) ->
                          {list(), binary()}.
dec_sequence(Content, Components) ->
    collection_dec(fun seq_parse_root/2, Content, Components).

%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 SEQUENCE OF and returns the binary.
%% NTCIP 1102 Section 2.3.9
%% @end
%%--------------------------------------------------------------------
-spec enc_sequence_of(Data :: list(), Type :: #type{}) -> iolist().
enc_sequence_of(Data, Type) ->
    Count = binary:encode_unsigned(length(Data)),
    Enc = [oerrt:encode_disp(Type, Value) || Value <- Data],
    % should switch call to unrestricted positive int. not enc_length
    [oerrt_primitive:push_length(Count), Enc].

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 SEQUENCE OF or SET binary and returns the decoded
%% value and remaining binary.
%% NTCIP 1102 Section 2.3.9
%% @end
%%--------------------------------------------------------------------
-spec dec_sequence_of(Content :: binary(), Type :: #type{}) ->
                             {list(), binary()}.
dec_sequence_of(Content, Type) ->
    {Count, Rest} = oerrt_primitive:pop_length(unsigned, Content),
    F = fun(T, C) -> oerrt:decode_disp(T, C) end,
    lists:mapfoldl(F, Rest, lists:duplicate(Count, Type)).

%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 SET and returns the binary.
%% NTCIP 1102 Section 2.3.10
%% @end
%%--------------------------------------------------------------------
-spec enc_set(Data :: list(), Components :: components()) -> iolist().
enc_set(Data, Components) ->
    F = fun(Type, Value) ->
                Enc = oerrt:encode_disp(Type, Value),
                [oerrt_primitive:enc_tag(Type), Enc] end,
    collection_enc(F, Data, Components).

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 SET binary and returns the decoded value and
%% remaining binary.
%% NTCIP 1102 Section 2.3.10
%% @end
%%--------------------------------------------------------------------
-spec dec_set(Content :: binary(), Components :: components()) ->
                     {list(), binary()}.
dec_set(Content, Components) ->
    collection_dec(fun set_parse_root/2, Content, Components).


%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 SET OF and returns the binary.
%% NTCIP 1102 Section 2.3.11
%% @end
%%--------------------------------------------------------------------
-spec enc_set_of(Data :: list(), Type :: #type{}) -> iolist().
enc_set_of(Data, Type) -> enc_sequence_of(Data, Type).

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 SET OF binary and returns the decoded
%% value and remaining binary.
%% NTCIP 1102 Section 2.3.11
%% @end
%%--------------------------------------------------------------------
-spec dec_set_of(Content :: binary(), Type :: #type{}) ->
                        {list() , binary()}.
dec_set_of(Content, Type) -> dec_sequence_of(Content, Type).

%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 CHOICE and returns the binary.
%% NTCIP 1102 Section 2.3.12
%% @end
%%--------------------------------------------------------------------
-spec enc_choice({Name :: atom(), Value :: term()},
                 Components :: component_list()) -> iolist().
enc_choice({Name, Value}, Components) ->
    Setup = choice_setup(Components),
    {Name, Tag, Type} = lists:keyfind(Name, 1, Setup),
    [oerrt_primitive:enc_tag(Tag), oerrt:encode_disp(Type, Value)].

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 CHOICE binary and returns the decoded value and
%% remaining binary.
%% NTCIP 1102 Section 2.3.12
%% @end
%%--------------------------------------------------------------------
-spec dec_choice(Content :: binary(), Components :: component_list()) ->
                        {term(), binary()}.
dec_choice(Content, Components) ->
    Setup = choice_setup(Components),
    {Tag, Rest1} = oerrt_primitive:dec_tag(Content),
    {Name, Tag, Type} = lists:keyfind(Tag, 2, Setup),
    {Value, Rest2} = oerrt:decode_disp(Type, Rest1),
    {{Name, Value}, Rest2}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an ASN.1 EMBEDDED PDV and returns the binary.
%% NTCIP 1102 Section 2.3.14
%% @end
%%--------------------------------------------------------------------
enc_embedded_pdv([Identification, StringValue]) ->
    [pdv_identification_enc(Identification),
     oerrt_primitive:enc_octet_string(StringValue, [])].

%%--------------------------------------------------------------------
%% @doc
%% Decodes an ASN.1 EMBEDDED PDV binary and returns the decoded value
%% and remaining binary.
%% NTCIP 1102 Section 2.3.14
%% @end
%%--------------------------------------------------------------------
dec_embedded_pdv(Content) ->
    {Identification, Rest1} = pdv_identification_dec(Content),
    {StringValue, Rest2} = oerrt_primitive:dec_octet_string(Rest1, []),
    {[Identification, StringValue], Rest2}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% Generic function to encode either SEQUENCE or SET types
%%--------------------------------------------------------------------
-spec collection_enc(F :: fun(), Data :: list(), components()) -> iodata().
collection_enc(F, Data, {Root1, Ext, Root2}) ->
    Setup = collection_esetup(F, Root1, Ext, Root2),
    Split = collection_split(Setup, Data),
    collection_ebuild(Setup, Split);
collection_enc(F, Data, {Root1, Ext}) ->
    collection_enc(F, Data, {Root1, Ext, []});
collection_enc(F, Data, Root) ->
    Fs = [collection_efun_root(F, R) || R <- Root],
    collection_ebuild_root(Fs, Data, <<>>).

%%--------------------------------------------------------------------
%% Creates an funtion for each #'ComponentType'{} to encode terms and
%% flag bitmap for Root and Ext component lists.
%%--------------------------------------------------------------------
-spec collection_esetup(F :: fun(),
                        component_list(),
                        component_list(),
                        component_list()) -> {[fun()], [fun()], [fun()]}.
collection_esetup(F, Root1, Ext, Root2) ->
    Root1Fs = [collection_efun_root(F, R) || R <- Root1],
    Root2Fs = [collection_efun_root(F, R) || R <- Root2],
    ExtGroup = ext_addition_group(Ext),
    {Root1Fs, [collection_efun_ext(E) || E <- ExtGroup], Root2Fs}.

%%--------------------------------------------------------------------
%% Split and rearranges Data to clump Root1 and Root2 together
%%--------------------------------------------------------------------
-spec collection_split({list(), list(), list()}, list()) -> {list(), list()}.
collection_split({Root1Fs, ExtFs, _Root2Fs}, Data) ->
    {N1, N2} = {length(Root1Fs), length(ExtFs)},
    {Root1Data, RestData} = lists:split(N1, Data),
    {ExtData, Root2Data} = lists:split(N2, RestData),
    {Root1Data ++ Root2Data, ExtData}.

%%--------------------------------------------------------------------
%% Creates a bare-bones OPTIONAL SEQUENCE type with the supplied
%% Components. This is to handle Addition Groups.
%%--------------------------------------------------------------------
-spec optional_sequence(component_list()) -> #'ComponentType'{}.
optional_sequence([Hd|_Tail]=Components) ->
    Type = #type{def=#'SEQUENCE'{components=Components}},
    Hd#'ComponentType'{prop='OPTIONAL', typespec=Type}.

%%--------------------------------------------------------------------
%% Converts all Extension Addition Groups in a component list into
%% OPTIONAL SEQUENCEs.
%%--------------------------------------------------------------------
-spec ext_addition_group([#'ComponentType'{}          |
                          'ExtensionAdditionGroupEnd' |
                          {'ExtensionAdditionGroup', _Name}]) -> components().
ext_addition_group([]) -> [];
ext_addition_group([{'ExtensionAdditionGroup', _Name}|Tail]) ->
    F = fun(Component) -> Component =/= 'ExtensionAdditionGroupEnd' end,
    {AdditionGroup, [_EndMarker|Rest]} = lists:splitwith(F, Tail),
    [optional_sequence(AdditionGroup) | ext_addition_group(Rest)];
ext_addition_group([Component|Tail]) ->
    [Component | ext_addition_group(Tail)].

%%--------------------------------------------------------------------
%% Creates a new function for encoding and creating bitmap for
%% OPTIONAL or DEFAULT #'ComponentType'{}
%%--------------------------------------------------------------------
-spec collection_efun_defopt(fun(), #type{}, term()) ->
                                    fun((term()) -> {<<_:1>>, iodata()}).
collection_efun_defopt(F, Type, DefOpt) ->
    fun(Value) when Value =:= DefOpt-> {<<0:1>>, <<>>};
       (Value)                      -> {<<1:1>>, F(Type, Value)} end.

%%--------------------------------------------------------------------
%% Generates function for encoding and creating bitmap for any
%% #'ComponentType'{}
%%--------------------------------------------------------------------
-spec collection_efun(fun(), <<_:1>> | <<>>, term(), #type{}) ->
                             fun((term()) -> {<<_:1>>, iodata()}).
collection_efun(F, _Bit, 'OPTIONAL', Type) ->
    collection_efun_defopt(F, Type, asn1_NOVALUE);
collection_efun(F, _Bit, {'DEFAULT', Default}, Type) ->
    collection_efun_defopt(F, Type, Default);
collection_efun(F, Bit, _Property, Type) ->
    fun(Value) -> {Bit, F(Type, Value)} end.

%%--------------------------------------------------------------------
%% Mandatory root #'ComponentType'{} do not have a bitmap flag
%%--------------------------------------------------------------------
-spec collection_efun_root(fun(), #'ComponentType'{}) ->
                                  fun((term()) -> {<<_:1>>, iodata()}).
collection_efun_root(F, #'ComponentType'{prop=Property, typespec=Type}) ->
    collection_efun(F, <<>>, Property, Type).

%%--------------------------------------------------------------------
%% Mandatory ext #'ComponentType'{} have a <<N:1>> bitmap flag
%%--------------------------------------------------------------------
-spec collection_efun_ext(#'ComponentType'{}) ->
                                 fun((term()) -> {<<_:1>>, iodata()}).
collection_efun_ext(#'ComponentType'{prop=Property, typespec=Type}) ->
    collection_efun(fun collection_efun_ext/2, <<1:1>>, Property, Type).

%%--------------------------------------------------------------------
%% Ext components encode the byte length of the encoded value
%%--------------------------------------------------------------------
-spec collection_efun_ext(#type{}, term()) -> iolist().
collection_efun_ext(Type, Value) ->
    Enc =  oerrt:encode_disp(Type, Value),
    oerrt_primitive:push_length(Enc).

%%--------------------------------------------------------------------
%% Encode and combine Ext and Root components
%%--------------------------------------------------------------------
-spec collection_ebuild({[fun()], [fun()], [fun()]}, {list(), list()}) ->
                               iolist().
collection_ebuild({Root1Fs, ExtFs, Root2Fs}, {RootData, ExtData}) ->
    {ExtMarker, Ext} = collection_ebuild_ext(ExtFs, ExtData),
    [collection_ebuild_root(Root1Fs ++ Root2Fs, RootData, ExtMarker), Ext].

%%--------------------------------------------------------------------
%% Encode Root components
%%--------------------------------------------------------------------
-spec collection_ebuild_root([fun()], list(), bitstring()) -> iolist().
collection_ebuild_root(Fs, Data, Bitmap0) ->
    {Bitmap1, Fields} = collection_ebuild(Fs, Data, Bitmap0, []),
    PadLen = oerrt_primitive:pad_length(bit_size(Bitmap1)),
    [<<Bitmap1/bits, 0:PadLen>>, Fields].

%%--------------------------------------------------------------------
%% Encode Ext components
%%--------------------------------------------------------------------
-spec collection_ebuild_ext([fun()], list())-> {<<_:1>>, iolist()}.
collection_ebuild_ext(Fs, Data) ->
    {Bitmap, Fields} = collection_ebuild(Fs, Data, <<>>, []),
    Size = bit_size(Bitmap),
    case <<0:Size>> =:= Bitmap of
        true  -> {<<0:1>>, []};
        false -> {<<1:1>>, [oerrt_primitive:enc_bitstring(Bitmap, []), Fields]}
    end.

%%--------------------------------------------------------------------
%% Builds bitmap and encoded values for Root and Ext components
%%--------------------------------------------------------------------
-spec collection_ebuild(list(), list(), bitstring(), iolist()) ->
                               {bitstring(), iolist()}.
collection_ebuild([], [], Bitmap, Fields) -> {Bitmap, Fields};
collection_ebuild([F|Fs], [Value|Data], Bitmap, Fields) ->
    {Bit, Enc} = F(Value),
    collection_ebuild(Fs, Data, <<Bitmap/bits, Bit/bits>>, [Fields, Enc]).

%%--------------------------------------------------------------------
%% Decode function for SEQUENCE
%%--------------------------------------------------------------------
seq_parse_root(TupleList, Content) ->
    lists:mapfoldl(fun collection_dfun/2, Content, TupleList).

%%--------------------------------------------------------------------
%% Decode function for SET
%%--------------------------------------------------------------------
set_parse_root(TupleList0, Content) ->
    F = fun(T) -> tuple_size(T) =:= 3 end,
    {TupleList1, Data} = lists:partition(F, TupleList0),
    set_parse_root(TupleList1, Data, Content).

set_parse_root([], Data, Content) -> {Data, Content};
set_parse_root(TupleList0, Data, Content) ->
    {Tag, Rest1} = oerrt_primitive:dec_tag(Content),
    {value, {Tag, Order, Type}, TupleList1} = lists:keytake(Tag, 1, TupleList0),
    {Value, Rest2} = oerrt:decode_disp(Type, Rest1),
    set_parse_root(TupleList1, [{Order, Value} | Data], Rest2).

collection_dec(F, <<N:1, Content/bits>>, {Root1, Ext, Root2}) ->
    {RootSetup, ExtGroup} = collection_dsetup(Root1 ++ Root2, Ext),
    {RootData, Rest2} = collection_parse_root(F, RootSetup, Content),
    {ExtData, Rest3} = collection_parse_ext(N, ExtGroup, Rest2),
    {collection_data_sort(RootData ++ ExtData), Rest3};
collection_dec(F, Content, {Root1, Ext}) ->
    collection_dec(F, Content, {Root1, Ext, []});
collection_dec(F, Content, Root) ->
    RootSetup = collection_dsetup_root(0, Root),
    {RootData, Rest} = collection_parse_root(F, RootSetup, Content),
    {collection_data_sort(RootData), Rest}.

collection_dsetup(Root, Ext) ->
    ExtGroup = [E || E <- ext_addition_group(Ext)],
    {collection_dsetup_root(1, Root), ExtGroup}.

collection_dsetup_root(ExtMarker, Root) ->
    NonMandatory = [R || R <- Root, R#'ComponentType'.prop =/= mandatory],
    BitmapLen = length(NonMandatory),
    PadLen = oerrt_primitive:pad_length(BitmapLen + ExtMarker),
    {BitmapLen, PadLen, Root}.

collection_parse_root(SetSeqF, {BitmapLen, PadLen, Root}, Content) ->
    <<Bitmap:BitmapLen/bits, 0:PadLen, Rest/bytes>> = Content,
    F = fun(Comp, Bits) -> collection_dbitmap(root, Comp, Bits) end,
    {TupleList, <<>>} = lists:mapfoldl(F, Bitmap, Root),
    SetSeqF(TupleList, Rest).

collection_parse_ext(0, Ext, Content) ->
    {[{E#'ComponentType'.textual_order, asn1_NOVALUE} || E <- Ext], Content};
collection_parse_ext(1, Ext, Content) ->
    {Bitmap1, Rest1} = oerrt_primitive:dec_bitstring(Content, []),
    F = fun(Comp, Bits) -> collection_dbitmap(ext, Comp, Bits) end,
    {TupleList, Bitmap2} = lists:mapfoldl(F, Bitmap1, Ext),
    {Data, Rest2} = lists:mapfoldl(fun collection_dfun_ext/2, Rest1, TupleList),
    {Data, collection_pop_unknown(Bitmap2, Rest2)}.

collection_dfun_ext(Tuple, Content) when tuple_size(Tuple) =:= 3 ->
    {_BitSize, Rest} = oerrt_primitive:dec_length(Content),
    collection_dfun(Tuple, Rest);
collection_dfun_ext(Tuple, Content) -> {Tuple, Content}.

collection_dfun({_Tag, Order, Type}, Content) ->
    {Value, Rest} = oerrt:decode_disp(Type, Content),
    {{Order, Value}, Rest};
collection_dfun(Tuple, Content) -> {Tuple, Content}.

collection_pop_unknown(<<>>, Content) -> Content;
collection_pop_unknown(<<_N:1, Bitmap/bits>>, Content) ->
    {_Unknown, Rest} = oerrt_primitive:pop_length(binary, Content),
    collection_pop_unknown(Bitmap, Rest).

collection_dbitmap(RootExt, ComponentType, Bitmap0) ->
    #'ComponentType'{prop=Property, textual_order=Order} = ComponentType,
    case collection_defopt(RootExt, Bitmap0, Property) of
        {Bitmap1, Value} -> {{Order, Value}, Bitmap1};
        Bitmap1 ->
            #'ComponentType'{typespec=Type, tags=[Tag]} = ComponentType,
            {{Tag, Order, Type}, Bitmap1}
    end.

collection_property_value('OPTIONAL')           -> asn1_NOVALUE;
collection_property_value({'DEFAULT', Default}) -> Default.

collection_defopt(ext, <<>>, _Property)    -> {<<>>, asn1_NOVALUE};
collection_defopt(root, Bitmap, mandatory) -> Bitmap;
collection_defopt(_RootExt, <<0:1, Bitmap/bits>>, Property) ->
    {Bitmap, collection_property_value(Property)};
collection_defopt(_RootExt, <<1:1, Bitmap/bits>>, _Property) -> Bitmap.

collection_data_sort(Data) ->
    [Value || {_Order, Value} <- lists:keysort(1, Data)].

%% -------------------------------------------------------------------
%% 2.3.12 CHOICE Value
%% -------------------------------------------------------------------
choice_setup(Components) ->
    F = fun(#'ComponentType'{name=Name, typespec=Type, tags=[Tag]}) ->
                {Name, Tag, Type} end,
    [F(C) || C <- Components].

%% -------------------------------------------------------------------
%% 2.3.14 EMBEDDED-PDV
%% -------------------------------------------------------------------
pdv_identification_enc({syntaxes, [Abstract, Transfer]}) ->
    [128, oerrt_primitive:enc_oid(Abstract), oerrt_primitive:enc_oid(Transfer)];
pdv_identification_enc({syntax, Value}) ->
    [129, oerrt_primitive:enc_oid(Value)];
pdv_identification_enc({'presentation-context-id', Value}) ->
    [130, oerrt_primitive:enc_integer_signed(Value)];
pdv_identification_enc({'context-negotiation', [Presentation, Transfer]}) ->
    [131, oerrt_primitive:enc_integer_signed(Presentation),
     oerrt_primitive:enc_oid(Transfer)];
pdv_identification_enc({'transfer-syntax', Value}) ->
    [132, oerrt_primitive:enc_oid(Value)];
pdv_identification_enc({fixed, Value}) ->
    [133, oerrt_primitive:enc_null(Value)].

pdv_identification_dec(<<128, Content/bytes>>) ->
    {Abstract, Rest1} = oerrt_primitive:dec_oid(Content),
    {Transfer, Rest2} = oerrt_primitive:dec_oid(Rest1),
    {[Abstract, Transfer], Rest2};
pdv_identification_dec(<<129, Content/bytes>>) ->
    oerrt_primitive:dec_oid(Content);
pdv_identification_dec(<<130, Content/bytes>>) ->
    oerrt_primitive:pop_length(signed, Content);
pdv_identification_dec(<<131, Content/bytes>>) ->
    {Presentation, Rest1} = oerrt_primitive:pop_length(signed, Content),
    {Transfer, Rest2} = oerrt_primitive:dec_oid(Rest1),
    {[Presentation, Transfer], Rest2};
pdv_identification_dec(<<132, Content/bytes>>) ->
    oerrt_primitive:dec_oid(Content);
pdv_identification_dec(<<133, Content/bytes>>) ->
    oerrt_primitive:dec_null(Content).
