%%% @author  Matthew
%%% @copyright (C) 2017 Matthew Bond
%%% @doc
%%% Realtime OER encoding and decoding of primitive ASN.1 Types
%%% @end
%%%-------------------------------------------------------------------
-module(oerrt_primitive).

-include_lib("asn1/src/asn1_records.hrl").

-type tag_class() :: 'UNIVERSAL' | 'APPLICATION' | 'CONTEXT' | 'PRIVATE'.
-type int_constraint() :: {signed | unsigned, 1 | 2 | 4 | unbounded}.

-compile([export_all]).

-export([enc_tag/1, dec_tag/1,
         push_length/1, pop_length/2,
         dec_length/1,
         enc_integer_signed/1,
         enc_integer/2, dec_integer/2,
         enc_integer/3, dec_integer/3,
         enc_enumerated/2, dec_enumerated/2,
         enc_real/1, dec_real/1,
         enc_bitstring/2, dec_bitstring/2,
         enc_bitstring/3, dec_bitstring/3,
         pad_length/1,
         enc_octet_string/2, dec_octet_string/2,
         enc_null/1, dec_null/1,
         enc_oid/1, dec_oid/1,
         enc_relative_oid/1, dec_relative_oid/1,
         enc_restricted_string/2, dec_restricted_string/2,
         enc_utf8_string/2, dec_utf8_string/2,
         enc_bmp_string/2, dec_bmp_string/2,
         enc_universal_string/2, dec_universal_string/2
        ]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 identifier tag.
%% NTCIP 1102:2005 Section 2.2.2
%% @end
%%--------------------------------------------------------------------
-spec enc_tag(#type{} | #tag{} | {tag_class(), integer()}) -> binary().
enc_tag(#type{tag=[Tag]})                 -> enc_tag(Tag);
enc_tag(#tag{class=Class, number=Number}) -> enc_tag({Class, Number});
enc_tag({Class, Number}) ->
    ClassNum = class_to_num(Class),
    case Number < 63 of
        true  -> <<ClassNum:2, Number:6>>;
        false -> [<<ClassNum:2, 63:6>> | multi_octet_enc(Number, [])]
    end.

%%--------------------------------------------------------------------
%% @doc
%% Decodes an OER ASN.1 identifier tag.
%% NTCIP 1102:2005 Section 2.2.2
%% @end
%%--------------------------------------------------------------------
-spec dec_tag(binary()) -> {{tag_class(), integer()}, binary()}.
dec_tag(<<Class:2, 63:6, Content/bytes>>) ->
    {Num, Rest} = multi_octet_dec(0, Content),
    {{num_to_class(Class), Num}, Rest};
dec_tag(<<Class:2, Num:6, Content/bytes>>) ->
    {{num_to_class(Class), Num}, Content}.

%%--------------------------------------------------------------------
%% @doc
%% Appends an OER ASN.1 length value to an iolist().
%% NTCIP 1102:2005 Section 2.2.3
%% @end
%%--------------------------------------------------------------------
-spec push_length(Content :: iodata()) -> iolist().
push_length(Content) ->
    case iolist_size(Content) of
        Length when Length < 128 -> [<<0:1, Length:7>>, Content];
        Length ->
            Encoded = binary:encode_unsigned(Length),
            ByteSize = byte_size(Encoded),
            [<<1:1, ByteSize:7>>, Encoded, Content]
    end.

%%--------------------------------------------------------------------
%% @doc
%% Decodes the length value from the binary and returns a binary of
%% the specified size from the front of the binary in addition to any
%% remaining binary.
%% NTCIP 1102:2005 Section 2.2.3
%% @end
%%--------------------------------------------------------------------
-spec pop_length(signed | unsigned | binary, binary()) ->
                        {integer() | binary(), binary()}.
pop_length(BinType, Content) ->
    {ByteSize, Rest}  = dec_length(Content),
    pop_length(BinType, ByteSize, Rest).

%%--------------------------------------------------------------------
%% @doc
%% Decodes an OER ASN.1 length value and returns the remaining binary.
%% NTCIP 1102:2005 Section 2.2.3
%% @end
%%--------------------------------------------------------------------
-spec dec_length(binary()) -> {integer(), binary()}.
dec_length(<<0:1, ByteSize:7, Rest/bytes>>) ->
    {ByteSize, Rest};
dec_length(<<1:1, OctetSize:7, Content/bytes>>) ->
    BitSize = 8 * OctetSize,
    <<ByteSize:BitSize, Rest/bytes>> = Content,
    {ByteSize, Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 BOOLEAN and returns the binary.
%% NTCIP 1102:2005 Section 2.3.1
%% @end
%%--------------------------------------------------------------------
-spec enc_boolean(false | true) -> <<_:8>>.
enc_boolean(false) -> <<0>>;
enc_boolean(true)  -> <<1>>.

%%--------------------------------------------------------------------
%% @doc
%% Decodes an OER ASN.1 BOOLEAN binary and returns the decoded value and
%% remaining binary.
%% NTCIP 1102:2005 Section 2.3.1
%% @end
%%--------------------------------------------------------------------
-spec dec_boolean(binary()) -> {false | true, binary()}.
dec_boolean(<<0, Rest/bytes>>) -> {false, Rest};
dec_boolean(<<_, Rest/bytes>>) -> {true, Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 signed unbounded INTEGER and returns the
%% binary.
%% NTCIP 1102:2005 Section 2.4.2
%% @end
%%--------------------------------------------------------------------
-spec enc_integer_signed(integer()) -> iolist().
enc_integer_signed(Number) ->
    ByteSize = twos_bytesize(Number, 0, []),
    oerrt_primitive:push_length(<<Number:ByteSize/unit:8>>).

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 INTEGER and returns the binary.
%% NTCIP 1102:2005 Section 2.3.2
%% @end
%%--------------------------------------------------------------------
-spec enc_integer(Int :: integer(), Constraint :: [tuple()]) -> iodata().
enc_integer(Int, Constraint) ->
    case integer_constraint(Constraint) of
        {signed, unbounded}   -> enc_integer_signed(Int);
        {unsigned, unbounded} -> enc_integer_unsigned(Int);
        {_Sign, ByteSize}     -> <<Int:ByteSize/unit:8>>
    end.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 INTEGER if a NamedList is defined and returns
%% the binary.
%% NTCIP 1102:2005 Section 2.3.2
%% @end
%%--------------------------------------------------------------------
-spec enc_integer(Int :: integer(), Constraint ::[tuple()],
                  NamedList :: [tuple()]) -> iodata().
enc_integer(Int, Constraint, _NamedList) when is_integer(Int) ->
    enc_integer(Int, Constraint);
enc_integer(Name, Constraint, NamedList) ->
    {Name, Integer} = lists:keyfind(Name, 1, NamedList),
    enc_integer(Integer, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the integer value from the binary and returns integer in
%% addition to any remaining binary.
%% NTCIP 1102:2005 Section 2.3.2
%% @end
%%--------------------------------------------------------------------
-spec dec_integer(Content :: binary(), Constraint :: [tuple()]) ->
                         {integer(), binary()}.
dec_integer(Content, Constraint) ->
    case integer_constraint(Constraint) of
        {Sign, unbounded} -> pop_length(Sign, Content);
        {signed, ByteSize} ->
            <<Int:ByteSize/signed-unit:8, Rest/bytes>> = Content,
            {Int, Rest};
        {unsigned, ByteSize} ->
            <<Int:ByteSize/unsigned-unit:8, Rest/bytes>> = Content,
            {Int, Rest}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Decodes the integer value from the binary if a NamedList is defined
%% and returns integer in addition to any remaining binary.
%% NTCIP 1102:2005 Section 2.3.2
%% @end
%%--------------------------------------------------------------------
-spec dec_integer(Content :: binary(), Constraint :: [tuple()],
                  NamedList :: [tuple()]) -> {integer(), binary()}.
dec_integer(Content, Constraint, NamedList) ->
    {Value, Rest} = dec_integer(Content, Constraint),
    case lists:keyfind(Value, 2, NamedList) of
        {Name, Value} -> {Name, Rest};
        false         -> {Value, Rest}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 enumerated value to an iolist().
%% NTCIP 1102:2005 Section 2.3.3
%% @end
%%--------------------------------------------------------------------
-spec enc_enumerated(atom() | integer(), {[tuple()], [tuple()]} | [tuple]) ->
                            iolist().
enc_enumerated(Key, {Root, Ext}) ->
    enc_enumerated(Key, Root ++ Ext);
enc_enumerated(Key, Values) when is_atom(Key) ->
    {Key, Enum} = lists:keyfind(Key, 1, Values),
    enc_enumerated(Enum, Values);
enc_enumerated(Enum, Values) ->
    case lists:max([enum_byte_size(E) || {_K, E} <- Values]) of
        0        -> [Enum];
        ByteSize -> [<<1:1, ByteSize:7, Enum:ByteSize/unit:8>>]
    end.

%%--------------------------------------------------------------------
%% @doc
%% Decodes the enumerated value from the binary and returns the key in
%% addition to any remaining binary.
%% NTCIP 1102:2005 Section 2.3.3
%% @end
%%--------------------------------------------------------------------
-spec dec_enumerated(Content :: binary(), {[tuple()], [tuple()]} | [tuple]) ->
                            {integer(), binary()}.
dec_enumerated(Content, {Root, Ext}) ->
    dec_enumerated(Content, Root ++ Ext);
dec_enumerated(<<0:1, Enum:7, Rest/bytes>>, Values) ->
    {Key, Enum} = lists:keyfind(Enum, 2, Values),
    {Key, Rest};
dec_enumerated(<<1:1, ByteSize:7/signed, Content/bytes>>, Values) ->
    <<Enum:ByteSize/signed-unit:8, Rest/bytes>> = Content,
    {Key, Enum} = lists:keyfind(Enum, 2, Values),
    {Key, Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 REAL and returns the binary.
%% NTCIP 1102:2005 Section 2.3.2
%% @end
%%--------------------------------------------------------------------
-spec enc_real(Real :: string()) -> iolist().
enc_real(Real) ->
    push_length(Real).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the real value from the binary and returns any remaining
%% binary.
%% NTCIP 1102:2005 Section 2.3.4
%% @end
%%--------------------------------------------------------------------
-spec dec_real(Content :: binary()) -> {string(), binary()}.
dec_real(Content) ->
    {Real, Rest} = pop_length(binary, Content),
    {binary_to_list(Real), Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 BIT STRING and returns the binary.
%% NTCIP 1102:2005 Section 2.3.5
%% @end
%%--------------------------------------------------------------------
-spec enc_bitstring(bitstring(), [tuple()]) -> iolist().
enc_bitstring(BitString, Constraint) ->
    case lists:keyfind('SizeConstraint', 1, Constraint) of
        {'SizeConstraint', {0, 0}} -> [];
        {'SizeConstraint', {N, N}} -> pad_bitstring(N, BitString);
        _SizeConstraint->
            BitSize = bit_size(BitString),
            PadLen = pad_length(BitSize),
            push_length([PadLen, <<BitString/bits, 0:PadLen>>])
    end.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 BIT STRING using a NamedList and returns the
%% binary.
%% NTCIP 1102:2005 Section 2.3.5
%% @end
%%--------------------------------------------------------------------
-spec enc_bitstring([atom() | {bit, integer()}] | bitstring(), [tuple()],
                    [{atom, integer()}]) -> iolist().
enc_bitstring(Names, Constraint, NamedList) when is_list(Names)->
    BitFlags = [name_to_bitflag(N, NamedList) || N <- Names],
    MaxFlag = lists:max(BitFlags),
    AccuFlags = fun(Flag, Accum) -> (1 bsl (MaxFlag - Flag)) bor Accum end,
    BitString = lists:foldl(AccuFlags, 0, BitFlags),
    enc_bitstring(<<BitString:(MaxFlag + 1)>>, Constraint);
enc_bitstring(BitString, Constraint, _NamedList) ->
    enc_bitstring(BitString, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the bitstring from the binary and returns any remaining
%% binary.
%% NTCIP 1102:2005 Section 2.3.5
%% @end
%%--------------------------------------------------------------------
-spec dec_bitstring(binary(), [tuple()]) -> {bitstring(), binary()}.
dec_bitstring(Content, Constraint) ->
    case lists:keyfind('SizeConstraint', 1, Constraint) of
        {'SizeConstraint', {0, 0}} -> {<<>>, Content};
        {'SizeConstraint', {N, N}} ->
            PadLen = pad_length(N),
            <<BitString:N/bits, 0:PadLen, Rest/bytes>> = Content,
            {BitString, Rest};
        _SizeConstraint->
            {<<PadLen, Bin/bytes>>, Rest} = pop_length(binary, Content),
            BitSize = bit_size(Bin) - PadLen,
            <<BitString:BitSize/bits, 0:PadLen>> = Bin,
            {BitString, Rest}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Decodes the bitstring from the binary, matches any names from the
%% NamedList and returns any remaining binary.
%% NTCIP 1102:2005 Section 2.3.5
%% @end
%%--------------------------------------------------------------------
-spec dec_bitstring(binary(), [tuple()], [{atom(), integer}]) ->
                           {bitstring() | [atom()], binary()}.
dec_bitstring(Content, Constraint, []) ->
    dec_bitstring(Content, Constraint);
dec_bitstring(Content, Constraint, NamedList) ->
    {BitString, Rest} = dec_bitstring(Content, Constraint),
    BitFlags = bitstring_to_flags(BitString, 0, []),
    KeyList = [lists:keyfind(Flag, 2, NamedList) || Flag <- BitFlags],
    {[Name || {Name, _Flag} <- KeyList], Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Returns number of bits required to pad a BitSized bitstring to
%% get byte alignment.
%% @end
%%--------------------------------------------------------------------
-spec pad_length(integer()) -> 0..8.
pad_length(BitSize) ->
    case BitSize rem 8 of
        0 -> 0;
        N -> 8 - N
    end.

%%--------------------------------------------------------------------
%% @doc
%% Encodes an OER ASN.1 OCTET STRING and returns the binary.
%% NTCIP 1102:2005 Section 2.3.6
%% @end
%%--------------------------------------------------------------------
enc_octet_string(OctetString, Constraint) ->
    encode_generic_string(fun list_to_binary/1, OctetString, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the octet string from the binary and returns any remaining
%% binary.
%% NTCIP 1102:2005 Section 2.3.6
%% @end
%%--------------------------------------------------------------------
dec_octet_string(Content, Constraint) ->
    decode_generic_string(fun binary_to_list/1, Content, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Encodes NULL and returns the binary.
%% NTCIP 1102:2005 Section 2.3.7
%% @end
%%--------------------------------------------------------------------
-spec enc_null(term()) -> [].
enc_null(_Value) -> [].

%%--------------------------------------------------------------------
%% @doc
%% Decodes NULL and returns the remaining binary.
%% NTCIP 1102:2005 Section 2.3.7
%% @end
%%--------------------------------------------------------------------
-spec dec_null(binary()) -> {'NULL', binary()}.
dec_null(Content) -> {'NULL', Content}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes NTCIP OER OID value and returns the binary.
%% NTCIP 1102:2005 Section 2.3.13
%% @end
%%--------------------------------------------------------------------
-spec enc_oid(nonempty_list(integer())) -> iolist().
enc_oid([X, Y|Tail]) ->
    First = (X * 40) + Y,
    Content = [multi_octet_enc(Sub, []) || Sub <- [First | Tail]],
    push_length(Content).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP OER OID value and returns the remaining binary.
%% NTCIP 1102:2005 Section 2.3.13
%% @end
%%--------------------------------------------------------------------
-spec dec_oid(binary()) -> {nonempty_list(integer()), binary()}.
dec_oid(Content) ->
    {OidEnc, Rest} = pop_length(binary, Content),
    [Hd | Tail] = oid_accu(OidEnc),
    Y = Hd rem 40,
    case (Hd - Y) div 40 of
        X when X > 2 -> {[2, (Hd - 80) | Tail], Rest};
        X            -> {[X, Y | Tail], Rest}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Encodes a NCTIP RELATIVE-OID and returns the binary.
%% RELATIVE-OID is not specified in NTCIP 1102:2005. However it is
%% required in NTCIP 1103:2016 Section 4.2.3 for SFMP Data Packets. The
%% encode/decode procedures are taken from NTCIP 1103:2016 Section
%% 4.2.4.4 which match the procedures outlined in ISO 8825-1:2015
%% Section 8.20.
%% @end
%%--------------------------------------------------------------------
-spec enc_relative_oid([integer()]) -> iolist().
enc_relative_oid(Data) ->
    Content = [multi_octet_enc(Sub, []) || Sub <- Data],
    push_length(Content).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP RELATIVE-OID value and returns the remaining binary.
%% See the comment above enc_relative_oid.
%% @end
%%--------------------------------------------------------------------
-spec dec_relative_oid(binary()) -> {[integer()], binary()}.
dec_relative_oid(Content) ->
    {OidEnc, Rest} = pop_length(binary, Content),
    {oid_accu(OidEnc), Rest}.

%%--------------------------------------------------------------------
%% @doc
%% Encodes the NTCIP OER Restricted Character String and returns the
%% binary.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec enc_restricted_string(string(), [tuple()]) -> iolist().
enc_restricted_string(Data, Constraint) -> enc_octet_string(Data, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP OER Character String and returns the
%% remaining binary.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec dec_restricted_string(binary(), [tuple()]) -> {string(), binary()}.
dec_restricted_string(Content, Constraint) ->
    dec_octet_string(Content, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Encodes the NTCIP OER UTF8String and returns the binary.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec enc_utf8_string(string(), [tuple()]) -> iolist().
enc_utf8_string(UTF8String, Constraint) ->
    F = fun unicode:characters_to_binary/1,
    encode_generic_string(F, UTF8String, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP OER UTF8STRING and returns the remaining binary.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec dec_utf8_string(binary(), [tuple()]) -> {string(), binary()}.
dec_utf8_string(Content, Constraint) ->
    F = fun unicode:characters_to_list/1,
    decode_generic_string(F, Content, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Encodes the NTCIP OER BMPSTRING and returns the binary.
%% NTCIP 1102:2005 Section 2.3.15
%% I am not sure if the following UCS-2 (BMP) and UCS-4 (Universal)
%% encoding is correct. ISO 8825-1:2015 refers to IS0 10646:2003. I
%% cannot find a copy of that version. It appears from research online
%% that UCS-2 is a 2-byte fixed width and UCS-4 is a 4-byte fixed
%% width encoding of Unicode code points. It is unclear if a BYTE
%% ORDER MARKER (BOM) is required. The standard Erlang ASN.1 library
%% does not appear to use a BOM in encoding or decoding. For
%% consistency sake, I did not use a BOM in encoding or decoding. If
%% anyone knows anything different let me know. Its implementation
%% should not be difficult.
%% @end
%%--------------------------------------------------------------------
-spec enc_bmp_string(string(), [tuple()]) -> iolist().
enc_bmp_string(BmpString, Constraint) ->
    F = fun(Bmp) -> << <<N:16>> || N <- Bmp >> end,
    encode_generic_string(F, BmpString, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP OER BMPSTRING and returns the remaining binary.
%% See the comment above enc_bmp_string.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec dec_bmp_string(binary(), [tuple()]) -> {string(), binary()}.
dec_bmp_string(Content, Constraint) ->
    F = fun(Bmp) -> [N || <<N:16>>  <= Bmp] end,
    decode_generic_string(F, Content, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Encodes the NTCIP OER UNIVERSALSTRING and returns the binary.
%% See the comment above enc_bmp_string.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec enc_universal_string(string(), [tuple()]) -> iolist().
enc_universal_string(UniversalString, Constraint) ->
    F = fun(Universal) -> << <<N:32>> || N <- Universal >> end,
    encode_generic_string(F, UniversalString, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Decodes the NTCIP OER UNIVERSALSTRING and returns the remaining
%% binary.
%% See the comment above enc_bmp_string.
%% NTCIP 1102:2005 Section 2.3.15
%% @end
%%--------------------------------------------------------------------
-spec dec_universal_string(binary(), [tuple()]) -> {string(), binary()}.
dec_universal_string(Content, Constraint) ->
    F = fun(Universal) -> [N || <<N:32>>  <= Universal] end,
    decode_generic_string(F, Content, Constraint).

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% Encodes tags and OIDs that do not fit in a single byte. The last
%% byte has its highest order bit set to 0 to indicate the end of the
%% multi-octet. All other highest order bits in the multi-octet are
%% set at 1.
%% NTCIP 1102:2005 Section 2.2.2.2
%%--------------------------------------------------------------------
-spec multi_octet_enc(Num :: integer(), Content :: [binary()]) -> [binary()].
multi_octet_enc(0, Content) -> Content;
multi_octet_enc(Num, []) ->
    multi_octet_enc(Num bsr 7, [<<0:1, Num:7>>]);
multi_octet_enc(Num, Content) ->
    multi_octet_enc(Num bsr 7, [<<1:1, Num:7>> | Content]).

%%--------------------------------------------------------------------
%% Decodes tags and OIDs that do not fit in a single byte.
%% NTCIP 1102:2005 Section 2.2.2.2
%%--------------------------------------------------------------------
-spec multi_octet_dec(Sum :: integer(), binary()) -> {integer(), binary()}.
multi_octet_dec(Sum, <<0:1, Num:7, Rest/bytes>>) ->
    {(Sum bsl 7) + Num, Rest};
multi_octet_dec(Sum, <<1:1, Num:7, Rest/bytes>>) ->
    multi_octet_dec((Sum bsl 7) + Num, Rest).

%%--------------------------------------------------------------------
%% Converts tag_class() atom to number to be for encoding.
%% NTCIP 1102:2005 Table 2-1
%%--------------------------------------------------------------------
-spec class_to_num(tag_class()) -> 0 | 1 | 2 | 3.
class_to_num('UNIVERSAL')   -> 0;
class_to_num('APPLICATION') -> 1;
class_to_num('CONTEXT')     -> 2;
class_to_num('PRIVATE')     -> 3.

%%--------------------------------------------------------------------
%% Converts decoded tag class number to atom.
%% NTCIP 1102:2005 Table 2-1
%%--------------------------------------------------------------------
-spec num_to_class(0 | 1 | 2 | 3) -> tag_class().
num_to_class(0) -> 'UNIVERSAL';
num_to_class(1) -> 'APPLICATION';
num_to_class(2) -> 'CONTEXT';
num_to_class(3) -> 'PRIVATE'.

%%--------------------------------------------------------------------
%% Decodes the length value from the binary and returns a binary of
%% the specified size from the front of the binary in addition to any
%% remaining binary.
%% NTCIP 1102:2005 Section 2.2.3
%%--------------------------------------------------------------------
-spec pop_length(signed | unsigned| binary, integer(), binary()) ->
                        {integer() | binary(), binary()}.
pop_length(BinType, ByteSize, Content) ->
    case BinType of
        signed   -> <<Value:ByteSize/signed-unit:8, Rest/bytes>>   = Content;
        unsigned -> <<Value:ByteSize/unsigned-unit:8, Rest/bytes>> = Content;
        binary   -> <<Value:ByteSize/bytes, Rest/bytes>>           = Content
    end,
    {Value, Rest}.

%%--------------------------------------------------------------------
%% Encodes an OER ASN.1 unsigned unbounded INTEGER and returns the
%% binary.
%% NTCIP 1102:2005 Section 2.4.1
%%--------------------------------------------------------------------
-spec enc_integer_unsigned(integer()) -> iolist().
enc_integer_unsigned(Number) ->
    Bin = binary:encode_unsigned(Number),
    oerrt_primitive:push_length(Bin).

%%--------------------------------------------------------------------
%% Determine the sign and byte size of the integer as defined by the
%% constraint.
%% NTCIP 1102:2005 Section 2.3.2
%%--------------------------------------------------------------------
-spec integer_constraint(Constraint :: [tuple()]) -> int_constraint().
integer_constraint(Constraint) ->
    case lists:keyfind('ValueRange', 1, Constraint) of
        false -> {signed, unbounded};
        {'ValueRange', {Lower, Upper}} when Lower < 0 ->
            F = fun(Number) -> twos_bytesize(Number, 0, []) end,
            integer_constraint(F, signed, Lower, Upper);
        {'ValueRange', {Lower, Upper}}  ->
            F = fun(Number) -> byte_size(binary:encode_unsigned(Number)) end,
            integer_constraint(F, unsigned, Lower, Upper)
    end.

%%--------------------------------------------------------------------
%% Determine the byte size of the integer as defined by the lower and
%% upper bounds of the constraint.
%% NTCIP 1102:2005 Section 2.3.2
%%--------------------------------------------------------------------
-spec integer_constraint(F :: fun(), Sign ::  signed | unsigned,
                         Lower :: integer(), Upper :: integer()) ->
                                int_constraint().
integer_constraint(F, Sign, Lower, Upper) ->
    case max(F(Lower), F(Upper)) of
        ByteSize when ByteSize < 3 -> {Sign, ByteSize};
        ByteSize when ByteSize < 5 -> {Sign, 4};
        _ByteSize                  -> {Sign, unbounded}
    end.

%%--------------------------------------------------------------------
%% Returns the smallest byte size required to encode a signed integer
%% in twos complement form.
%% NTCIP 1102:2005 Section 2.4.2
%%--------------------------------------------------------------------
-spec twos_bytesize(Int :: integer(), Size :: integer(), Bytes :: list()) ->
                              integer().
twos_bytesize(-1, ByteSize, [Hd|_Tail]) when Hd > 127 -> ByteSize;
twos_bytesize( 0, ByteSize, [Hd|_Tail]) when Hd < 128 -> ByteSize;
twos_bytesize(Int, ByteSize, Bytes) ->
    twos_bytesize(Int bsr 8, ByteSize + 1, [Int band 255 | Bytes]).


%%--------------------------------------------------------------------
%% Returns the smallest byte size required to encode the enumerated
%% value.
%% NTCIP 1102:2005 Section 2.3.3
%%--------------------------------------------------------------------
-spec enum_byte_size(Enum :: integer()) -> integer().
enum_byte_size(Enum) when Enum >= 0, Enum < 128 -> 0;
enum_byte_size(Enum) -> twos_bytesize(Enum, 0, []).

%%--------------------------------------------------------------------
%% Converts a name from the NamedList to the specified bitflag. Any
%% bitflag can be selected by submitteing {bit, BitFlag}.
%%--------------------------------------------------------------------
-spec name_to_bitflag({bit, integer} | atom(), [tuple()]) -> integer().
name_to_bitflag({bit, BitFlag}, _NamedList) -> BitFlag;
name_to_bitflag(Name, NamedList) ->
    {Name, BitFlag} = lists:keyfind(Name, 1, NamedList),
    BitFlag.

%%--------------------------------------------------------------------
%% Returns a bitstring right-padded with zeroes to get byte alignment.
%%--------------------------------------------------------------------
-spec pad_bitstring(N :: integer(), Bitstring :: bitstring()) -> binary().
pad_bitstring(N, BitString) ->
    PadLen = pad_length(N),
    <<BitString/bits, 0:PadLen>>.

%%--------------------------------------------------------------------
%% Takes a bitstring and returns a list of integer positions where the
%% flag was set to 1.
%%--------------------------------------------------------------------
-spec bitstring_to_flags(bitstring(), integer(), [integer()]) -> [integer()].
bitstring_to_flags(<<>>, _Index, Flags) -> lists:reverse(Flags);
bitstring_to_flags(<<0:1, Rest/bits>>, Index, Flags) ->
    bitstring_to_flags(Rest, Index + 1, Flags);
bitstring_to_flags(<<1:1, Rest/bits>>, Index, Flags) ->
    bitstring_to_flags(Rest, Index + 1, [Index | Flags]).

%%--------------------------------------------------------------------
%% Generic function that encodes OCTET, UTF8, BMP and UNIVERSAL
%% strings.
%%--------------------------------------------------------------------
encode_generic_string(F, String, Constraint) ->
    case lists:keyfind('SizeConstraint', 1, Constraint) of
        {'SizeConstraint', {0, 0}} -> [];
        {'SizeConstraint', {N, N}} -> F(String);
        _SizeConstraint ->
            Bin = F(String),
            push_length(Bin)
    end.

%%--------------------------------------------------------------------
%% Generic function that decodes OCTET, UTF8, BMP and UNIVERSAL
%% strings.
%%--------------------------------------------------------------------
decode_generic_string(F, Content, Constraint) ->
    case lists:keyfind('SizeConstraint', 1, Constraint) of
        {'SizeConstraint', {0, 0}} -> {[], Content};
        {'SizeConstraint', {N, N}} ->
            {Bin, Rest} = pop_length(binary, N, Content),
            {F(Bin), Rest};
        _SizeConstraint ->
            {Bin, Rest} = pop_length(binary, Content),
            {F(Bin), Rest}
    end.


%%--------------------------------------------------------------------
%% Accumulate decoded OID values from a binary.
%%--------------------------------------------------------------------
-spec oid_accu(binary()) -> list(integer()).
oid_accu(<<>>) -> [];
oid_accu(Content) ->
    {Value, Rest} = multi_octet_dec(0, Content),
    [Value | oid_accu(Rest)].
