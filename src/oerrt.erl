%%%-------------------------------------------------------------------
%%% @author  Matthew
%%% @copyright (C) 2017 Matthew Bond
%%% @doc
%%% Interface module for the realtime encoding and decoding of
%%% messages in the ASN.1 Octet Encoding Rules (OER) format as defined
%%% in NTCIP 1102
%%% @end
%%%-------------------------------------------------------------------
-module(oerrt).

-include_lib("asn1/src/asn1_records.hrl").

-type constructed() :: #'SEQUENCE'{}             |
                       #'SET'{}                  |
                       {'CHOICE',     [#type{}]} |
                       {'SEQUENCE OF', #type{}}  |
                       {'SET OF',      #type{}}.

%% API
-export([encode/2, decode/2, encode_disp/2, decode_disp/2, build_type/3]).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Takes list of Erlang terms and returns the OER encoded binary.
%% @end
%%--------------------------------------------------------------------
-spec encode(Type :: #type{}, Data :: list()) -> binary().
encode(Type, Data) ->
    Result = encode_disp(Type, Data),
    iolist_to_binary(Result).

%%--------------------------------------------------------------------
%% @doc
%% Takes an OER encoded binary and returns a list of Erlang terms.
%% @end
%%--------------------------------------------------------------------
-spec decode(Type :: #type{}, Content :: binary()) -> list().
decode(Type, Content) ->
    {Result, <<>>} = decode_disp(Type, Content),
    Result.

%%--------------------------------------------------------------------
%% @doc
%% Dispatches the Data to the correct encode function based on the
%% ASN.1 definition.
%% @end
%%--------------------------------------------------------------------
-spec encode_disp(#type{}, Data :: [term()]) -> iodata().
encode_disp(#type{def='BOOLEAN'}, Data) ->
    oerrt_primitive:enc_boolean(Data);
encode_disp(#type{def={'INTEGER', NamedList}, constraint=Constraint}, Data) ->
    oerrt_primitive:enc_integer(Data, Constraint, NamedList);
encode_disp(#type{def='INTEGER', constraint=Constraint}, Data) ->
    oerrt_primitive:enc_integer(Data, Constraint);
encode_disp(#type{def={'ENUMERATED', Values}}, Data) ->
    oerrt_primitive:enc_enumerated(Data, Values);
encode_disp(#type{def='REAL'}, Data) ->
    oerrt_primitive:enc_real(Data);
encode_disp(#type{def={'BIT STRING', NamedList}, constraint=Constraint}, Data) ->
    oerrt_primitive:enc_bitstring(Data, Constraint, NamedList);
encode_disp(#type{def='OCTET STRING', constraint=Constraint}, Data) ->
    oerrt_primitive:enc_octet_string(Data, Constraint);
encode_disp(#type{def='NULL'}, Data) ->
    oerrt_primitive:enc_null(Data);
encode_disp(#type{def=#'SEQUENCE'{components=Components}}, Data) ->
    oerrt_constructed:enc_sequence(Data, Components);
encode_disp(#type{def={'SEQUENCE OF', Type}}, Data) ->
    oerrt_constructed:enc_sequence_of(Data, Type);
encode_disp(#type{def=#'SET'{components=Components}}, Data) ->
    oerrt_constructed:enc_set(Data, Components);
encode_disp(#type{def={'SET OF', Type}}, Data) ->
    oerrt_constructed:enc_set_of(Data, Type);
encode_disp(#type{def={'CHOICE', Components}}, {Name, Value}) ->
    oerrt_constructed:enc_choice({Name, Value}, Components);
encode_disp(#type{def='OBJECT IDENTIFIER'}, Data) ->
    oerrt_primitive:enc_oid(Data);
encode_disp(#type{def='RELATIVE-OID'}, Data) ->
    oerrt_primitive:enc_relative_oid(Data);
encode_disp(#type{def='UniversalString', constraint=Constraint}, Data) ->
    oerrt_primitive:enc_universal_string(Data, Constraint);
encode_disp(#type{def='BMPString', constraint=Constraint}, Data) ->
    oerrt_primitive:enc_bmp_string(Data, Constraint);
encode_disp(#type{def='UTF8String', constraint=Constraint}, Data) ->
    oerrt_primitive:enc_utf8_string(Data, Constraint);
encode_disp(#type{def=Def, constraint=Constraint}, Data)
  when Def =:= 'NumericString';
       Def =:= 'PrintableString';
       Def =:= 'TeletexString';
       Def =:= 'T61String';
       Def =:= 'VideotexString';
       Def =:= 'IA5String';
       Def =:= 'UTCTime';
       Def =:= 'GeneralizedTime';
       Def =:= 'GraphicString';
       Def =:= 'VisibleString';
       Def =:= 'GeneralString' ->
    oerrt_primitive:enc_restricted_string(Data, Constraint).

%% Need to add dispatch support for PDV and CHARACTER STRING types
%% once I understand what they are and what they do.

%%--------------------------------------------------------------------
%% @doc
%% Dispatches the Data to the correct decode function based on the
%% ASN.1 Type definition.
%% @end
%%--------------------------------------------------------------------
-spec decode_disp(#type{}, Content :: binary()) ->
          {list(), binary()}.
decode_disp(#type{def='BOOLEAN'}, Data) ->
    oerrt_primitive:dec_boolean(Data);
decode_disp(#type{def={'INTEGER', NamedList}, constraint=Constraint}, Data) ->
    oerrt_primitive:dec_integer(Data, Constraint, NamedList);
decode_disp(#type{def='INTEGER', constraint=Constraint}, Data) ->
    oerrt_primitive:dec_integer(Data, Constraint);
decode_disp(#type{def={'ENUMERATED', Values}}, Data) ->
    oerrt_primitive:dec_enumerated(Data, Values);
decode_disp(#type{def='REAL'}, Data) ->
    oerrt_primitive:dec_real(Data);
decode_disp(#type{def={'BIT STRING', NamedList}, constraint=Constraint}, Data) ->
    oerrt_primitive:dec_bitstring(Data, Constraint, NamedList);
decode_disp(#type{def='OCTET STRING', constraint=Constraint}, Data) ->
    oerrt_primitive:dec_octet_string(Data, Constraint);
decode_disp(#type{def='NULL'}, Data) ->
    oerrt_primitive:dec_null(Data);
decode_disp(#type{def=#'SEQUENCE'{components=Components}}, Data) ->
    oerrt_constructed:dec_sequence(Data, Components);
decode_disp(#type{def={'SEQUENCE OF', Type}}, Data) ->
    oerrt_constructed:dec_sequence_of(Data, Type);
decode_disp(#type{def=#'SET'{components=Components}}, Data) ->
    oerrt_constructed:dec_set(Data, Components);
decode_disp(#type{def={'SET OF', Type}}, Data) ->
    oerrt_constructed:dec_set_of(Data, Type);
decode_disp(#type{def={'CHOICE', Components}}, Data) ->
    oerrt_constructed:dec_choice(Data, Components);
decode_disp(#type{def='OBJECT IDENTIFIER'}, Data) ->
    oerrt_primitive:dec_oid(Data);
decode_disp(#type{def='RELATIVE-OID'}, Data) ->
    oerrt_primitive:dec_relative_oid(Data);
decode_disp(#type{def='UniversalString', constraint=Constraint}, Data) ->
    oerrt_primitive:dec_universal_string(Data, Constraint);
decode_disp(#type{def='BMPString', constraint=Constraint}, Data) ->
    oerrt_primitive:dec_bmp_string(Data, Constraint);
decode_disp(#type{def='UTF8String', constraint=Constraint}, Data) ->
    oerrt_primitive:dec_utf8_string(Data, Constraint);
decode_disp(#type{def=Def, constraint=Constraint}, Data)
  when Def =:= 'NumericString';
       Def =:= 'PrintableString';
       Def =:= 'TeletexString';
       Def =:= 'T61String';
       Def =:= 'VideotexString';
       Def =:= 'IA5String';
       Def =:= 'UTCTime';
       Def =:= 'GeneralizedTime';
       Def =:= 'GraphicString';
       Def =:= 'VisibleString';
       Def =:= 'GeneralString' ->
    oerrt_primitive:dec_restricted_string(Data, Constraint).

%%--------------------------------------------------------------------
%% @doc
%% Builds the ASN.1 Type definitionfrom the *.asn1db files,
%% including the full Type definition of all
%% #'Externaltypereference'{}.
%% @end
%%--------------------------------------------------------------------
-spec build_type(Folder :: string(), Module :: module(), TypeName :: atom()) ->
                        #type{}.
build_type(Folder, Module, TypeName) ->
    asn1_db:dbstart([Folder]),
    asn1_db:dbload(Module),
    #typedef{typespec=Type} = asn1_db:dbget(Module, TypeName),
    asn1_db:dbstop(),
    build_type(Folder, Type).

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% Builds the ASN.1 Type definition from the *.asn1db files,
%% including the full Type definitions of all
%% #'Externaltypereference'{}.
%%--------------------------------------------------------------------
-spec build_type(Folder :: string(), #type{}) -> #type{}.
build_type(Folder, #type{def=Def}=Type) ->
    Inner = asn1ct_gen:get_inner(Def),
    case asn1ct_gen:type(Inner) of
        #'Externaltypereference'{module=ExtModule, type=ExtTypeName} ->
            build_type(Folder, ExtModule, ExtTypeName);
        {constructed, bif} ->
            Type#type{def=build_constructed(Folder, Def)};
        _Other -> Type
    end.

%%--------------------------------------------------------------------
%% Iterates through all #'ComponentType'{} in SET, SEQUENCE, or CHOICE
%% types to lookup any #'Externaltypereference' and insert the full
%% definition.
%%--------------------------------------------------------------------
-spec build_components(Folder :: string(), Components :: list() | tuple()) ->
                              list() | tuple().
build_components(Folder, Components) when is_tuple(Components) ->
    List = tuple_to_list(Components),
    list_to_tuple([build_components(Folder, Comp) || Comp <- List]);
build_components(Folder, Components) ->
    F = fun(#'ComponentType'{typespec=Type} = Comp) ->
                Comp#'ComponentType'{typespec=build_type(Folder, Type)};
           (Other) -> Other end,
    [F(Comp) || Comp <- Components].

%%--------------------------------------------------------------------
%% Builds a constructed type; SET, SEQUENCE, and CHOICE have a list of
%% #'ComponentType'{} whereas SET OF and SEQUENCE OF has a single
%% #type{}.
%%--------------------------------------------------------------------
-spec build_constructed(Folder :: string(), Constructed :: constructed()) ->
                               constructed().
build_constructed(Folder, #'SEQUENCE'{components=Components}=Seq) ->
    Seq#'SEQUENCE'{components=build_components(Folder, Components)};
build_constructed(Folder, #'SET'{components=Components}=Set) ->
    Set#'SET'{components=build_components(Folder, Components)};
build_constructed(Folder, {'CHOICE', Components}) ->
    {'CHOICE', build_components(Folder, Components)};
build_constructed(Folder, {SeqSetOf, Type}) ->
    {SeqSetOf, build_type(Folder, Type)}.
