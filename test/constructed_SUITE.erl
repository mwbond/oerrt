-module(constructed_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("asn1/src/asn1_records.hrl").

-export([all/0, init_per_suite/1, end_per_suite/1]).
-export([seq1/1, seq2/1, seq3/1, seq35/1, seq4/1, seq5/1,
         set1/1, set2/1, set5/1,
         seqof1/1, seqof2/1,
         setof1/1, setof2/1,
         choice1/1, choice2/1
         ]).

all() ->
    [seq1, seq2, seq3, seq35, seq4, seq5,
     set1, set2, set5,
     seqof1, seqof2,
     setof1, setof2,
     choice1, choice2
    ].

init_per_suite(Config) ->
    Dir = ?config(data_dir, Config),
    FileName = filename:join(Dir, "Constructed.asn1"),
    asn1ct:compile(FileName, [verbose, noobj, {outdir, Dir}]),
    Config.

end_per_suite(_Config) -> ok.

roundtrip(TypeDef, Value, Encoded) ->
    enc_test(TypeDef, Value, Encoded),
    dec_test(TypeDef, Value, Encoded).

enc_test(TypeDef, Value, Encoded) ->
    Encoded = oerrt:encode(TypeDef, Value).

dec_test(TypeDef, Value, Encoded) ->
    Value = oerrt:decode(TypeDef, Encoded).

seq1(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq1'),
    roundtrip(TypeDef, ["NTCIP", 5], <<"NTCIP", 1, 5>>).

seq2(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq2'),
    roundtrip(TypeDef, ["NTCIP", 7, 255], <<16#40, "NTCIP", 2, 0, 255>>),
    roundtrip(TypeDef, ["NTCIP", 5, 255], <<16#C0, "NTCIP", 5, 2, 0, 255>>),
    roundtrip(TypeDef, ["NTCIP", 5, asn1_NOVALUE], <<16#80, "NTCIP", 5>>).

seq3(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq3'),
    roundtrip(TypeDef, ["NTCIP", 5], <<0, "NTCIP", 1, 5>>),
    dec_test(TypeDef, ["NTCIP", 5],
              <<128, "NTCIP", 1, 5, 2, 7, 128, 5, 4, "TEST">>).
seq35(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq35'),
    dec_test(TypeDef, ["NTCIP", asn1_NOVALUE, 5], <<0, "NTCIP", 1, 5>>).

seq4(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq4'),
    roundtrip(TypeDef, ["NTCIP", <<16#18>>, "TEST", 5, 120],
              <<16#C0, "NTCIP", 5, 1, 120, 2, 6,
                16#C0, 1, 16#18, 5, 4, "TEST">>),
    dec_test(TypeDef, ["NTCIP", <<0>>, asn1_NOVALUE, 7, 5],
             <<128, "NTCIP", 1, 5, 2, 7, 128, 1, 0>>).

seq5(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Seq5'),
    roundtrip(TypeDef, [55, high, [22, "NTCIP"], [20, "UVA"]],
              <<128, 55, 2, 5, 224, 1, 1, 7, 192, 22,
                "NTCIP", 5, 128, 20, "UVA">>),
    roundtrip(TypeDef, [55, high, asn1_NOVALUE, [20, "UVA"]],
              <<128, 55, 2, 5, 160, 1, 1, 5, 128, 20, "UVA">>).

set1(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Set1'),
    roundtrip(TypeDef, ["NTCIP", 5], <<128, "NTCIP", 129, 1, 5>>).

set2(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Set2'),
    dec_test(TypeDef, ["NTCIP", 5, 255],
             <<16#C0, 129, 5, 130, 2, 0, 255, 128, "NTCIP">>),
    dec_test(TypeDef, ["NTCIP", 7, 255],
             <<16#40, 130, 2, 0, 255, 128, "NTCIP">>),
    dec_test(TypeDef, ["NTCIP", 5, asn1_NOVALUE],
             <<16#80, 128, "NTCIP", 129, 5>>).
set5(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Set5'),
    roundtrip(TypeDef, [55, high, asn1_NOVALUE, [20, "UVA"]],
              <<128, 128, 55, 2, 5, 160, 1, 1, 5, 128, 20, "UVA">>).

seqof1(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'SeqOf1'),
    roundtrip(TypeDef, [4, 7, 8, 9, 6], <<1, 5, 4, 7, 8, 9, 6>>).

seqof2(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'SeqOf2'),
    io:format("~p~n", [TypeDef]),
    roundtrip(TypeDef,
              [["NTCIP", 5, 255],
               ["NTCIP", 7, 255],
               ["NTCIP", 5, asn1_NOVALUE]],
              <<1, 3, 16#C0, "NTCIP", 5, 2, 0, 255,
                16#40, "NTCIP", 2, 0, 255,
                16#80, "NTCIP", 5>>).

setof1(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'SetOf1'),
    roundtrip(TypeDef, [4, 7, 8, 9, 6], <<1, 5, 4, 7, 8, 9, 6>>).

setof2(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'SetOf2'),
    roundtrip(TypeDef,
              [["NTCIP", 5, 255],
               ["NTCIP", 7, 255],
               ["NTCIP", 5, asn1_NOVALUE]],
              <<1, 3, 16#C0, "NTCIP", 5, 2, 0, 255,
                16#40, "NTCIP", 2, 0, 255,
                16#80, "NTCIP", 5>>).

choice1(Config) ->
    TypeDef0 = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Choice0'),
    roundtrip(TypeDef0, {'obj1', 5}, <<2:2, 63:6, 63, 1, 5>>),
    roundtrip(TypeDef0, {'obj3', 5}, <<1:2, 63:6, 65, 1, 5>>),
    roundtrip(TypeDef0, {'obj4', 5}, <<3:2, 63:6, 66, 1, 5>>),
    roundtrip(TypeDef0, {'obj5', 5}, <<0:2, 63:6, 67, 1, 5>>),
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Choice1'),
    roundtrip(TypeDef, {'obj1', 5}, <<128, 1, 5>>),
    roundtrip(TypeDef, {'obj2', "NTCIP"}, <<129, "NTCIP">>).

choice2(Config) ->
    TypeDef = oerrt:build_type(?config(data_dir, Config), 'Constructed', 'Choice2'),
    roundtrip(TypeDef, {'obj2', "NTCIP"}, <<129, "NTCIP">>),
    roundtrip(TypeDef, {'obj4', {'obj5', "NTCIP"}}, <<131, 128, "NTCIP">>),
    roundtrip(TypeDef, {'obj4', {'obj6', true}}, <<131, 129, 1>>).
