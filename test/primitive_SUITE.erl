-module(primitive_SUITE).

-include_lib("common_test/include/ct.hrl").
-include_lib("asn1/src/asn1_records.hrl").

-export([all/0, init_per_suite/1, end_per_suite/1]).
-export([boolean_test/1,
         null_test/1,
         integer_test/1,
         enumerated_test/1,
         real_test/1,
         bitstring_test/1,
         octet_string_test/1,
         oid_test/1,
         restricted_string_test/1
         ]).

all() ->
    [boolean_test,
     null_test,
     integer_test,
     enumerated_test,
     real_test,
     bitstring_test,
     octet_string_test,
     oid_test,
     restricted_string_test
    ].

init_per_suite(Config) ->
    Dir = ?config(data_dir, Config),
    FileName = filename:join(Dir, "Primitive.asn1"),
    asn1ct:compile(FileName, [verbose, noobj, {outdir, Dir}]),
    F = fun(Name) -> oerrt:build_type(Dir, 'Primitive', Name) end,
    [{f, F} | Config].

end_per_suite(_Config) -> ok.

roundtrip(TypeDef, Value, Encoded) ->
    enc_test(TypeDef, Value, Encoded),
    dec_test(TypeDef, Value, Encoded).

enc_test(TypeDef, Value, Encoded) ->
    Encoded = oerrt:encode(TypeDef, Value).

dec_test(TypeDef, Value, Encoded) ->
    Value = oerrt:decode(TypeDef, Encoded).

boolean_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Boolean'),  false, <<0>>),
    roundtrip(F('Boolean'),  true,  <<1>>),
    dec_test(F('Boolean'),   true,  <<9>>).

null_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Null1'),  'NULL', <<>>).

integer_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Integer'),  127, <<16#01, 16#7F>>),
    roundtrip(F('Integer'),  128, <<16#02, 16#00, 16#80>>),
    roundtrip(F('Integer'), -128, <<16#01, 16#80>>),
    roundtrip(F('Integer'), -129, <<16#02, 16#FF, 16#7F>>),
    roundtrip(F('Int1U1'),     5, <<16#05>>),
    roundtrip(F('Int1U2'),     5, <<16#05>>),
    roundtrip(F('Int1Nl'),     5, <<16#05>>),
    roundtrip(F('Int1Nl'),   red, <<16#00>>),
    roundtrip(F('Int2U1'),     5, <<16#00, 16#05>>),
    roundtrip(F('Int2U2'),     5, <<16#00, 16#05>>),
    roundtrip(F('Int4U1'),     5, <<16#00, 16#00, 16#00, 16#05>>),
    roundtrip(F('Int4U2'),     5, <<16#00, 16#00, 16#00, 16#05>>),
    roundtrip(F('IntXU1'),     5, <<16#01, 16#05>>),
    roundtrip(F('Int1S1'),    -5, <<16#FB>>),
    roundtrip(F('Int1S2'),    -5, <<16#FB>>),
    roundtrip(F('Int2S1'),    -5, <<16#FF, 16#FB>>),
    roundtrip(F('Int2S2'),    -5, <<16#FF, 16#FB>>),
    roundtrip(F('Int2S3'),    -5, <<16#FF, 16#FB>>),
    roundtrip(F('Int2S4'),    -5, <<16#FF, 16#FB>>),
    roundtrip(F('Int4S1'),    -5, <<16#FF, 16#FF, 16#FF, 16#FB>>),
    roundtrip(F('Int4S2'),    -5, <<16#FF, 16#FF, 16#FF, 16#FB>>),
    roundtrip(F('Int4S3'),    -5, <<16#FF, 16#FF, 16#FF, 16#FB>>),
    roundtrip(F('Int4S4'),    -5, <<16#FF, 16#FF, 16#FF, 16#FB>>),
    roundtrip(F('IntXS1'),    -5, <<16#01, 16#FB>>),
    roundtrip(F('IntXS2'),    -5, <<16#01, 16#FB>>),
    roundtrip(F('IntXS3'),    -5, <<16#01, 16#FB>>),
    roundtrip(F('IntXNl'), black, <<16#01, 16#FB>>).

enumerated_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Enum1'),    white,   <<16#01>>),
    roundtrip(F('Enum1Ext'), green,   <<16#03>>),
    roundtrip(F('Enum3'),    white,   <<16#82, 16#00, 16#01>>),
    roundtrip(F('Enum3Ext'), yellow,  <<16#82, 16#00, 16#80>>),
    roundtrip(F('Enum3Neg'), magenta, <<16#81, 16#FF>>).

real_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Real'), "3.14", <<4, $3, $., $1, $4>>),
    roundtrip(F('Real'), "2.345e12",
                                   <<8, $2, $., $3, $4, $5, $e, $1, $2>>).

bitstring_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('BitString1'), <<16, 0, 0:4>>, <<4, 4, 16, 0, 0>>),
    roundtrip(F('BitString2'), <<177:12>>,     <<177:12, 0:4>>),
    roundtrip(F('BitString3'), <<16, 0:6>>,    <<3, 2, 16, 0>>),
    roundtrip(F('BitString3'), <<16, 0, 0:4>>, <<4, 4, 16, 0, 0>>),
    roundtrip(F('BitString4'), <<>>, <<>>),
    roundtrip(F('BitString5'), [foo, bar, gnome], <<2, 4, 2#11010000>>),
    roundtrip(F('BitString5'), [gnu, punk], <<3, 1, 2#00100000, 2#00000010>>),
    enc_test(F('BitString5'), [{bit, 2}, {bit, 14}],
             <<3, 1, 2#00100000, 2#00000010>>).

octet_string_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('OctetString1'), "NTCIP", <<5, "NTCIP">>),
    roundtrip(F('OctetString2'), "NTCIP", <<5, "NTCIP">>),
    roundtrip(F('OctetString3'), "NTCIP", <<"NTCIP">>),
    roundtrip(F('OctetString4'), [],      <<>>),
    roundtrip(F('OctetString1'), lists:duplicate(128, 0),
              <<129, 128, 0:(8 * 128)>>).

oid_test(Config) ->
    F = ?config(f, Config),
    Part1 = <<16#0D, 16#2B, 16#06, 16#01, 16#04, 16#01, 16#89>>,
    Part2 = <<16#36, 16#04, 16#01, 16#03, 16#01, 16#01, 16#03>>,
    Result = [1,3,6,1,4,1,1206,4,1,3,1,1,3],
    roundtrip(F('OID1'), Result, <<Part1/bytes, Part2/bytes>>),
    roundtrip(F('OID1'), [2, 999, 3], <<3, 16#883703:24>>),
    roundtrip(F('Relative'), [8571, 3, 2], <<4, 16#C27B:16, 3, 2>>).

restricted_string_test(Config) ->
    F = ?config(f, Config),
    roundtrip(F('Numeric'),     "1234",      <<4, "1234">>),
    roundtrip(F('Printable'),   "Printable", <<9, "Printable">>),
    roundtrip(F('Teletex'),     "Teletex",   <<7, "Teletex">>),
    roundtrip(F('T61'),         "T61",       <<3, "T61">>),
    roundtrip(F('Videotex'),    "Videotex",  <<8, "Videotex">>),
    roundtrip(F('Ia5'),         "Ia5",       <<3, "Ia5">>),
    roundtrip(F('Utc'),         "97100211-0500",    <<13, "97100211-0500">>),
    roundtrip(F('Generalized'), "19971002103130.5", <<16, "19971002103130.5">>),
    roundtrip(F('Graphic'),     "Graphic",  <<7, "Graphic">>),
    roundtrip(F('Visible'),     "Visible",  <<7, "Visible">>),
    roundtrip(F('General'),     "General",  <<7, "General">>),
    roundtrip(F('Universal'),   "Univ",     <<16, 0, 0, 0, "U",
                                                  0, 0, 0, "n",
                                                  0, 0, 0, "i",
                                                  0, 0, 0, "v">>),
    roundtrip(F('Bmp'),         "BMP",      <<6, 0, "B", 0, "M", 0, "P">>),
    roundtrip(F('Utf8'),        "€UTF8",    <<7, "€UTF8"/utf8>>).
